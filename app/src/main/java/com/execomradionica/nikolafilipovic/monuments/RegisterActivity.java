package com.execomradionica.nikolafilipovic.monuments;

import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Nikola Filipović on 23.5.2016..
 */
@EActivity(R.layout.activity_register)
public class RegisterActivity extends AppCompatActivity {

    @ViewById
    EditText email2;

    @ViewById
    EditText password2;

    @ViewById
    EditText password3;

    @Click
    void register2(){
        Toast.makeText(this, "Got no time for this", Toast.LENGTH_SHORT).show();
    }
}
