package com.execomradionica.nikolafilipovic.monuments.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * Created by Nikola Filipović on 23.5.2016..
 */

@DatabaseTable(tableName = "monument")
public class Monument {

        public static final String ID_FIELD_NAME = "id";

        @DatabaseField(columnName = ID_FIELD_NAME, generatedId = true)
        private int id;

        @DatabaseField(columnName = "user", canBeNull = false, foreign = true)
        private User user;

        @DatabaseField(columnName = "description", canBeNull = false)
        private String description;

        @DatabaseField(columnName = "imageUrl", canBeNull = false)
        private String imageUrl;

        @DatabaseField(columnName = "name", canBeNull = false)
        private String name;


        public Monument(User user, String description, String imageUrl, String name) {
            this.user = user;
            this.description = description;
            this.imageUrl = imageUrl;
            this.name = name;
        }

        public int getId() {
        return id;
    }

        public void setId(int id) {
        this.id = id;
    }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

}
