package com.execomradionica.nikolafilipovic.monuments;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    @ViewById
    EditText email;

    @ViewById
    EditText password;

    @Click
    void login(){
        HomeActivity_.intent(this).start();
    }

    @Click
    void register(){
        RegisterActivity_.intent(this).start();
    }

}
