package com.execomradionica.nikolafilipovic.monuments.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * Created by Nikola Filipović on 23.5.2016..
 */

@DatabaseTable(tableName = "user")
public class User {
        public static final String ID_FIELD_NAME = "id";

        @DatabaseField(columnName = ID_FIELD_NAME, generatedId = true)
        private int id;

        @DatabaseField(columnName = "name", canBeNull = false)
        private String name;

        @DatabaseField(columnName = "email", canBeNull = false, unique = true)
        private String email;

        @DatabaseField(columnName = "password", canBeNull = false)
        private String password;


        public User() {
        }

        public User(String name, String password, String email) {
            this.name = name;
            this.password = password;
            this.email = email;
        }

        public int getId() {
        return id;
    }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String username) {
            this.name = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }



}

