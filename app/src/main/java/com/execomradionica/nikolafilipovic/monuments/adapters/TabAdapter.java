package com.execomradionica.nikolafilipovic.monuments.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.execomradionica.nikolafilipovic.monuments.fragments.AllMonumentsFragment_;
import com.execomradionica.nikolafilipovic.monuments.fragments.MyMonumentsFragment_;

/**
 * Created by Nikola Filipović on 23.5.2016..
 */
public class TabAdapter extends FragmentStatePagerAdapter {

    private static final int MAX_TABS = 3;

    private Context context;

    public TabAdapter(AppCompatActivity activity) {
        super(activity.getSupportFragmentManager());
        this.context = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return AllMonumentsFragment_.builder().build();
            case 1:
                return MyMonumentsFragment_.builder().build();
            case 2:
                return MyMonumentsFragment_.builder().build();
                //return MonumentTypesFragment_.builder().build();
            default:
                throw new RuntimeException("Invalid tab number");
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Home";
            case 1:
                return "My monuments";
            case 2:
                return "Monument types";
            default:
                throw new RuntimeException("Invalid tab number");
        }
    }

    @Override
    public int getCount() {
        return MAX_TABS;
    }
}
