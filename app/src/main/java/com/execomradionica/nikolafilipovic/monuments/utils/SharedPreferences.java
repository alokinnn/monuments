package com.execomradionica.nikolafilipovic.monuments.utils;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by Nikola Filipović on 23.5.2016..
 */

@SharedPref(SharedPref.Scope.UNIQUE)
public interface SharedPreferences {

    int id();
}
