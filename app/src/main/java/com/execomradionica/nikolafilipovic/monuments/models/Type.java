package com.execomradionica.nikolafilipovic.monuments.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Nikola Filipović on 23.5.2016..
 */

@DatabaseTable(tableName = "type")
public class Type {

    public static final String ID_FIELD_NAME = "id";

    @DatabaseField(columnName = ID_FIELD_NAME, generatedId = true)
    private int id;

    @DatabaseField(columnName = "typename", canBeNull = false, unique = true)
    private String typename;

    public Type(String typename) {
        this.typename = typename;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }
}
