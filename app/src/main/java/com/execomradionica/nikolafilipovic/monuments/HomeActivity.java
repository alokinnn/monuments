package com.execomradionica.nikolafilipovic.monuments;

import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.execomradionica.nikolafilipovic.monuments.adapters.TabAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Nikola Filipović on 23.5.2016..
 */

@EActivity(R.layout.activity_home)
public class HomeActivity extends AppCompatActivity{

    @ViewById
    TabLayout tabLayout;

    @ViewById
    ViewPager pager;

    TabAdapter tabAdapter;

    boolean backPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (backPressedOnce) {
            moveTaskToBack(true);
            super.onBackPressed();
            return;
        }

        this.backPressedOnce = true;
        Toast.makeText(this, "Click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                backPressedOnce = false;
            }
        }, 2000);
    }

    @OptionsItem(R.id.item_logout)
    void itemLogout() {
        LoginActivity_.intent(this).start();
        finish();
    }

    @AfterViews
    void setUpTabs() {
        tabAdapter = new TabAdapter(this);
        pager.setAdapter(tabAdapter);
        tabLayout.setupWithViewPager(pager);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position <= 1) {
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        }
        );
    }

    @Click
    void newMonument(){
        Toast.makeText(this, "Got no time for this", Toast.LENGTH_SHORT).show();
    }



}
